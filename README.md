# epicsarchiver-config

This repository is used to store all PVs to archive.
This is in **alpha stage** and only to evaluate the concept.


## Introduction

For each IOC, the list of PVs to archive should be saved in a file named `<ioc name>.archive`.
The file should be put under a directory named after the archiver appliance fully-qualified domain name.

Here is the list of current archiver appliances:

- **archiver-01.tn.esss.lu.se** for the LCR
- **archiver-02.tn.esss.lu.se** for TS2
- **ics-archiver11.tn.esss.lu.se** for testing

## Archive File format

The files shall include one PV name per line.
Empty lines and lines starting with "#" are allowed.

Here is an example:

```
# PV name
ISrc-010:PwrC-CoilPS-01:CurS
ISrc-010:PwrC-CoilPS-01:CurR
# Comments are allowed
LEBT-010:Vac-VCG-30000:PrsStatR
```

Note that the sampling period and method should be decided by the site policies.py script.
When passing them via the API, they take precendence over what is returned by the policies.
To avoid that, passing them is not supported anymore.
If present in the file (old format), the period and method are ignored.

## Workflow

When pushing to master, the PVs are automatically added to the archiver.

The `process_archives.py` script looks at files that changed since last commit.
All the PVs from those files are sent to the proper appliance for archiving.

PV deletion is currently not handled.
